# gnrc

A very simple demo of using GitLab CI/CD and Docker as a deployment pipeline.

See [the tutorial](./TUTORIAL.md) for more info!
