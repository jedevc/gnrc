# Tutorial

In this tutorial, we will:
- Talk (briefly) about why CI/CD is generally a good idea, and why we're
  making things seemingly more complicated with Docker
- Build a very basic CI/CD pipeline that deploys a basic Python application to
  a Digital Ocean server.
- Discuss future improvements you might want.

If you're just interested in the final results, then just browse the rest of
this repository!

So without further ado, let's get started!

## Why CI/CD?

The trend of using modern CI/CD pipelines is part of the larger DevOps
movement. The general idea of this movement is to merge the seperate roles of
development and operations into a single role, and to make application building
and deployment everyone's responsibility.

A few philosophies that the movement quite likes:
- Automation
    - People are terrible and make mistakes: processes should be automated as
      far as possible.
    - This applies to test automation, infrastructure management, app
      deployment, security scanning, code review, etc.
- Deploy fast, deploy often
    - Deploying bug fixes and new features should be effortless and easy. Many
      companies have embraced this and deploy every single commit on their main
      branch, up to hundreds of times a day in under 5 minutes.
- [Infrastructure as Code](https://en.wikipedia.org/wiki/Infrastructure_as_code)
    - All the state of a deployment, servers, dns records can and should all be
      represented as code. We won't quite go this far, but it's good to keep in
      mind that you *could* if you wanted.

CI/CD is one of the key parts of DevOps, and helps automate deployments - no
need to contact a separate ops team to get your code into production, if you
commit it, it's already on it's way. This helps teams move faster and with more
confidence.

## Why Docker?

Another important piece of the puzzle for DevOps is containerization (which
Docker popularized). You can think of a container as an uber-lightweight VM,
which contains a whole separate Linux distribution. So it's not just "your
application", but also all your dependencies, tools, etc!

This becomes *very* valuable in complex tech stacks with lots of different
things running - instead of needing to track your dependencies and manage those
in your deployment process, you instead get to build and ship *one* box per
part of your tech stack (e.g. one for frontend, one for backend).

Docker has become so ubiquitous that you often don't even need to worry about
what it runs on - in our case we'll run it ourselves on a Digital Ocean box,
but there are many other ways to run it!

## Our application

To actually deploy something, let's actually first build a dummy app to deploy.
We'll just steal the [hello world example from the flask documentation](https://flask.palletsprojects.com/en/2.0.x/quickstart/) and put it in `app.py`:

```python
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"
```

This will just serve a simple HTML page at the root of the site... and
literally nothing else.

We'll also make a simple requirements.txt file (which we'll use for listing our
dependencies for pip, python's package manager):

    $ echo "flask" >> requirements.txt

Awesome! If you want, you can run the app yourself:

    $ pip install -r requirements.txt
    $ flask run
     * Environment: production
       WARNING: This is a development server. Do not use it in a production deployment.
       Use a production WSGI server instead.
     * Debug mode: off
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)

### Dockerizing our app

So with our brand-new shiny app, let's package this up into a Docker image. The
Docker image is a template for a Docker container - you can think of this as
the difference between a VM image that you download from the internet to import
locally, and what the VM is when it's running.

To build this docker image, we're going to need a Dockerfile. The syntax is a
little funky, but it should be pretty clear. We'll go step-by-step.

```dockerfile
FROM python:3-alpine
```

This line defines our "base" image - kind of like inheritance in OOP. This base
image provides all the basics to do python stuff in it and comes with
everything pre-installed. The `:3-alpine` at the end is the "label" for that
image - you can see all the available labels for the python image at DockerHub
[here](https://hub.docker.com/_/python).

```dockerfile
WORKDIR /app
```

This line specifies our working directory - it'll create the `/app` directory,
and then all our commands from this point forward will be relative to `/app`.

```dockerfile
COPY requirements.txt .
RUN pip install -r requirements.txt
```

These two lines copy our requirements.txt from our local directory into our
working directory (`/app`), and then runs the pip command to install all of our
dependencies.

```dockerfile
COPY app.py .
```

Then we copy in our hello world application `app.py`.

```dockerfile
ENTRYPOINT flask run --host 0.0.0.0
```

Then finally, we set our app's entrypoint, the command that will be run when we
start up our container. This is just the same `flask run` as above, but we need
to manually set the `--host` flag to listen on *all* available network
interfaces to be able to see the results outside of the container.

That's it! Now we can actually build our container using docker just to check
everything works:

    $ docker build -t gnrc .

This creates a new image in `.` the current directory, and tags it with
`gnrc`.

We can run it using:

    $ docker run -it -p5000:5000 gnrc
     * Environment: production
       WARNING: This is a development server. Do not use it in a production deployment.
       Use a production WSGI server instead.
     * Debug mode: off
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)

### Defining our stack

So, now we have a basic Docker setup, we want to define our entire stack. Now
this is useful because we could imagine a setup where we might have multiple
docker images (e.g. one frontend image, one backend image, etc) and we want to
link them all together.

To do this, we can use a docker-compose.yaml file. Don't worry, it's very simple:

```yaml
version: '3'
services:
  gnrc:
    build: .
    image: gnrc:latest
    ports:
      - "80:5000"
```

This defines our service `gnrc` that uses our `gnrc` image (that can be built
using the `Dockerfile` in `.`), and defines what ports should be exposed - we
forward port 80 (the http port) on the host to port 5000 on the container
(where flask is listening).

We can run this using:

    $ docker-compose up -d
    
And stop it using:

    $ docker-compose down
    
## Setting up a server

Before going any further, make sure the dockerizing stuff has gone ok! Test it
locally as much as possible, since other stuff likely will go wrong in the next
couple steps.

We're going to setup a server so that our app has somewhere to run on the
public internet. There are many hosting providers out there, but Digital Ocean
is quite simple to get setup with, but the process will be nearly identical on
all of them. What we'll get will be a Linux VM with a public IP address, that
we can access and host servers on.

I won't walk through the exact process of setting up a server on Digital Ocean - I've
simply got their smallest droplet installed with Ubuntu 21.10, copied over my
personal ssh key so I can get access to it.

I've also setup the domain name `gnrc.uk` that I own to point to the IP address
I get from Digital Ocean, so we can setup TLS later.

So to get access to my server:

    $ ssh root@gnrc.uk

I'll use `#` as my prompt for when I'm on the server now, since I'm root. In a
"real-world" setup, you should probably create some other users, and use them
to keep things more secure. But for a tutorial, this will be fine.

First let's apply some updates:

    # apt update && apt upgrade -y

Now let's install docker and docker-compose:

    # apt install -y docker.io docker-compose

We'll also generate another set of ssh keys here, so that we can give access to
this machine from our GitLab pipeline in the next step.

    # ssh-keygen -t ed25519
    Generating public/private ed25519 key pair.
    Enter file in which to save the key (/root/.ssh/id_ed25519): ./gitlab_id_ed25519
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in ./gitlab_id_ed25519
    Your public key has been saved in ./gitlab_id_ed25519.pub

Then we can allow access for this key by appending the public part to the
authorized_keys file (make sure it's a double `>`, or you'll overwrite your own
key!):

    # cat ./gitlab_id_ed25519.pub >> .ssh/authorized_keys

We've now got our server setup! Make sure to copy the private key somewhere
safe for later (just don't share it with anyone).

## Setting up GitLab CI

We're going to use GitLab CI to trigger our server to run some tests on our
app, and then deploy it to our server.

GitLab CI works with "pipelines". A pipeline is just a collection of jobs and
their relationships - the pipeline will run on every commit to the main branch,
as well as allowing us to manually trigger it when we want to.

A "job" is simply a single task - if a job fails, then later jobs won't run.

Jobs are organized into "stages", with each stage completing before the next
stage will begin.

Jobs get picked up and run by "runners" - we could host these ourselves, or use
GitLab's shared runners, or whatever shared runners are available on your
self-hosted GitLab.

So we're going to create two simple jobs:
- An `app-import` job, which will check that our app doesn't have any glaring
  syntax errors, and that all our dependencies can be found.
- A `app-deploy` job, which will ssh into our server and update everything.

We write our GitLab CI config in a `.gitlab-ci.yml` file:

```yaml
stages:
- test
- deploy
```

At the top of our file, we define two stages - our "test" stage and our
"deploy" stage. We don't want to deploy if our tests fail!

```yaml
app-import:
  image: python:3
  stage: test
  before_script:
    - pip install -r requirements.txt
  script:
    - python -c "import app"
```

That's all! We define the `stage: test` to say it's in the test stage, and we
give it some scripts to run. The `python:3` might look familiar... that's
because it's a docker image, and GitLab CI uses docker under-the-hood for our
jobs!

Our scripts our broken up into two steps, mostly for readability. In the
`before_script` part, we install our requirements.txt, then in our `script`
part we do a basic test by just running a little python snippet to import our
app.

In a real world scenario, we'd really want a lot more different types of tests,
and we might choose to run those in parallel by creating a job for each of
them, but this will do for demonstration.

If we commit our changes, and push them to GitLab, we should see a pipeline
running now!

    $ git add .gitlab-ci.yml app.py docker-compose.yaml Dockerfile requirements.txt
    $ git commit -m "Add gitlab CI"
    $ git push

If you navigate to your pipelines page (e.g. mine is [here](https://gitlab.com/jedevc/gnrc/-/pipelines)),
you should be able to see your running pipeline! If you've followed everything
correctly, you should see the job pass! If it doesn't, take a look through the
logs and see if you can work our the problem!

### Deploy deploy deploy!

Now it's time to deploy! So, let's go and define our `app-deploy` job (make
sure to replace gnrc.uk where you need to, that's my site):

```yaml
app-deploy:
  image: ubuntu
  stage: deploy
  before_script:
    - apt update
    - apt install -y ssh
  script:
    - chmod 600 $GNRC_SSH_KEY
    - ssh -i $GNRC_SSH_KEY -o StrictHostKeyChecking=no root@gnrc.uk "cd /app && docker-compose down && rm -rf /app" || true
    - scp -i $GNRC_SSH_KEY -o StrictHostKeyChecking=no -r $PWD/ root@gnrc.uk:/app
    - ssh -i $GNRC_SSH_KEY -o StrictHostKeyChecking=no root@gnrc.uk "cd /app && docker-compose build && docker-compose up -d"
```

This time, we're going with a base `ubuntu` image, so that then in
`before_script` we can install SSH to access our server.

Now - for a moment, just ignore the `$GNRC_SSH_KEY` variable - we'll come back
to it in a moment, but for now, just trust me, it contains the path to that SSH
private key we made earlier.

So let's go through each step in the `script` one-by-one:

- `chmod 600 $GNRC_SSH_KEY`
  - Sets the right permissions on our SSH key, otherwise SSH will complain and
    not do any of the other steps.
- `ssh -i $GNRC_SSH_KEY -o StrictHostKeyChecking=no root@gnrc.uk "cd /server && docker-compose down && rm -rf /server || true"`
  - SSH into our server, then:
    - Navigate to our `/server` folder
    - Stop all our running containers
    - Remove the `/server` folder
  - The `|| true` means we won't fail if any of those steps fail, since the
    first time we run this, the `/server` directory won't exist.
- `scp -i $GNRC_SSH_KEY -o StrictHostKeyChecking=no -r $PWD/ root@gnrc.uk:/server`
  - Upload our current directory, which contains our git repository to the server.
- `ssh -i $GNRC_SSH_KEY -o StrictHostKeyChecking=no root@gnrc.uk "cd /server && docker-compose build && docker-compose up -d"`
  - SSH into our server, then:
    - Navigate to our `/server` folder
    - Build our docker images
    - Start all our containers!
    
If you commit this, then the pipeline should run again... but it'll fail!
Because obviously, we haven't actually set our `$GNRC_SSH_KEY`. This is part of
our credentials management - we should __never__ commit credentials to our git
repository, so we need some other way of managing them.

This is where GitLab CI/CD variables come in - in your project settings, under
CI/CD, there's a section called Variables. Create a new variable, called
`GNRC_SSH_KEY`, and paste your copied SSH private key here (make sure there's a
newline at the end), and make sure to change the type to "File".

After saving that, GitLab will inject the contents of that variable into your
CI/CD runners so that they can access the SSH private key they need to access
your server!

If you retrigger your failed pipeline, then (hopefully) your app should deploy
correctly! Go to http://your-site and you should see "Hello world".

## TLS

TLS is kind of important for modern sites, thankfully with this setup, it's
*very* easy to get it working. There's been a lot of innovation in this area
recently, and my new favourite [Caddy](https://caddyserver.com/) is absolutely
incredibly to use.

To use this, we're gonna modify our docker-compose.yaml file. Here's the full
modified version:

```yaml
version: '3'
services:
  gnrc:
    build: .
    image: gnrc:latest

  reverse_proxy:
    image: caddy:latest
    command:
      caddy reverse-proxy --from gnrc.uk --to gnrc:5000
    ports:
      - "80:80"
      - "443:443"
```

Note how we remove the port mappings from `gnrc` and instead add them to our
`reverse_proxy` container (as well as add 443, the port number for https).

Our reverse_proxy will take incoming requests, and pass them along to gnrc -
because caddy does automagic TLS, then our app doesn't need to know anything
about https, it just handles normal http, with caddy doing all the heavy
lifting!

The only new field in this new container is the `command` field: in this field,
we specify what command should get run in the container, so we instruct caddy
to start a reverse proxy from our domain to our local container.

Commit your changes, watch your pipeline, and hopefully you should have TLS!

## Conclusion

And that should be everything working... but there's still quite a way to go if
you want to do things the "proper" way (left as an exercise to the reader):

- We really shouldn't be building our docker images on the same machine we run
  them on!
  
  We can build them in GitLab CI using something like [buildkit](https://dille.name/blog/2020/06/01/using-buildkit-for-cloud-native-builds-in-gitlab/),
  and then pushing them to the GitLab/Digital Ocean container registry - then
  we just need to pull them on the server.
- We really shouldn't be using flask like that - it deliberately warns about
  not running that directly in production, so we should use something like
  [gunicorn](https://gunicorn.org/) to wrap flask inside our docker container.
- Using SSH in a gitlab pipeline is a bit of an expensive practice generally,
  since we're waiting on our own server to finish work, and we pay for runners
  by the minute - we should actually setup our server as a runner itself, and
  then not need to use SSH to get access, just run the deploy job directly on
  the server (this is a little fiddly to do)!
  
Additionally, there's lots more things you can and should do!

- Reduce our downtime in between bringing our containers down and bringing them
  back up - can you make it so your users won't even notice?
- Create separate frontend and backend containers!
- Create multi-stages builds for the Dockerfile, so that you can have both
  development and production versions of your setup, with things like
  hot-reloading in the development version so you can easily test.

However, this should be a "good-enough" framework to get started, and I'm
getting tired of writing words. Good luck!
